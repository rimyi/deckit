const initialState = {
    auth: false,
    nickname: '',
    uuid: null,
    players: [],
    servers: {}
};

export default initialState;
