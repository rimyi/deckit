const deck = {
    cdn: "http://localhost:5000",
    cards: [
        {
            "id":"1",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-artist-brush-102127.jpg"
        },
        {
            "id":"2",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/architecture-art-colorful-161154.jpg"
        },
        {
            "id":"3",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-creative-creativity-20967.jpg"
        },
        {
            "id":"4",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-graffiti-paint-36759.jpg"
        },
        {
            "id":"5",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-background-decoration-354939.jpg"
        },
        {
            "id":"6",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-art-materials-artistic-542556.jpg"
        },
        {
            "id":"7",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/adult-art-barren-417040.jpg"
        },
        {
            "id":"8",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/abstract-art-artistic-251287.jpg"
        },
        {
            "id":"9",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/ancient-ancient-civilization-arch-159862.jpg"
        },
        {
            "id":"10",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/abstract-art-graffiti-4776.jpg"
        },
        {
            "id":"11",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/architecture-art-bright-219000.jpg"
        },
        {
            "id":"12",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-beautiful-face-413793.jpg"
        },
        {
            "id":"13",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-black-and-white-exhibition-21264.jpg"
        },
        {
            "id":"14",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/anise-aroma-art-277253.jpg"
        },
        {
            "id":"15",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-asian-buffalo-46152.jpg"
        },
        {
            "id":"16",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/abstract-angelic-art-566641.jpg"
        },
        {
            "id":"17",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/actor-adult-ancient-236171.jpg"
        },
        {
            "id":"18",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-artistic-arts-and-crafts-395074.jpg"
        },
        {
            "id":"19",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-black-and-white-blur-39348.jpg"
        },
        {
            "id":"20",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-blue-boat-194094.jpg"
        },
        {
            "id":"21",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/adult-art-background-673862.jpg"
        },
        {
            "id":"22",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-backlit-dawn-595747.jpg"
        },
        {
            "id":"23",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-design-gallery-460736.jpg"
        },
        {
            "id":"24",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-artist-black-and-white-669319.jpg"
        },
        {
            "id":"25",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/architecture-art-artistic-163811.jpg"
        },    
        {
            "id":"26",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/abstract-architecture-art-227675.jpg"
        }, 
        {
            "id":"27",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/ancient-art-blur-219610.jpg"
        }, 
        {
            "id":"28",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/adult-art-black-and-white-604694.jpg"
        }, 
        {
            "id":"29",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-artistic-cabarete-63238.jpg"
        }, 
        {
            "id":"30",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/architectural-design-architecture-art-417270.jpg"
        }, 
        {
            "id":"31",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/abstract-alcohol-art-274131.jpg"
        }, 
        {
            "id":"32",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/abstract-art-artistic-206064.jpg"
        }, 
        {
            "id":"33",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/abstract-art-artistic-327509.jpg"
        }, 
        {
            "id":"34",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-carnival-celebration-35797.jpg"
        }, 
        {
            "id":"35",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/arch-architecture-art-332208.jpg"
        }, 
        {
            "id":"36",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/alley-architecture-art-459470.jpg"
        }, 
        {
            "id":"37",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/ancient-antique-architecture-304694.jpg"
        }, 
        {
            "id":"38",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/administration-america-art-345092.jpg"
        }, 
        {
            "id":"39",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/ancient-antique-architecture-280395.jpg"
        }, 
        {
            "id":"40",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/abstract-art-celebration-750843.jpg"
        }, 
        {
            "id":"41",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-artist-beautiful-933255.jpg"
        }, 
        {
            "id":"42",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/adult-art-back-view-220694.jpg"
        }, 
        {
            "id":"43",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/antique-art-board-264693.jpg"
        }, 
        {
            "id":"44",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/abstract-action-animal-305197.jpg"
        }, 
        {
            "id":"45",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/abstract-art-blur-389907.jpg"
        }, 
        {
            "id":"46",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-background-brick-272254.jpg"
        }, 
        {
            "id":"47",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/adult-art-artist-297648.jpg"
        }, 
        {
            "id":"48",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/adult-architecture-art-277054.jpg"
        }, 
        {
            "id":"49",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/art-blur-bokeh-459740.jpg"
        }, 
        {
            "id":"50",
            "title":"Cos tam",
            "img":this.cdn+"/cards/images/architecture-art-board-game-462180.jpg"
        }, 
    ]
}

module.exports = deck;